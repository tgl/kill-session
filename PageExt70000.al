pageextension 70000 "Kill Session" extends "Session List"
{
    layout
    {
    }

    actions
    {
        addafter("Debug Next Session")
        {

            action("Kill Session")
            {
                Caption = 'Kill Session';
                Image = Delete;
                Promoted = true;
                PromotedCategory = Category4;
                PromotedIsBig = true;
                ApplicationArea = All;

                trigger OnAction();
                var
                    KillSessionQst: label 'Kill Session?';
                begin
                    if confirm(KillSessionQst, false) then
                        StopSession("Session ID");
                end;
            }
        }
    }
}